<cfsetting enablecfoutputonly="true" />
<cfprocessingdirective pageencoding="utf-8" />
<!--- circuit: login --->
<!--- fuseaction: user --->
<cftry>
<cfset myFusebox.thisPhase = "appinit">
<cfset myFusebox.thisCircuit = "login">
<cfset myFusebox.thisFuseaction = "user">
<cfif myFusebox.applicationStart or
		not myFusebox.getApplication().applicationStarted>
	<cflock name="#application.ApplicationName#_fusebox_#FUSEBOX_APPLICATION_KEY#_appinit" type="exclusive" timeout="30">
		<cfif not myFusebox.getApplication().applicationStarted>
			<cfset myFusebox.getApplication().applicationStarted = true />
		</cfif>
	</cflock>
</cfif>
<!--- do action="musers.login_user" --->
<cfset myFusebox.thisPhase = "requestedFuseaction">
<cfset myFusebox.thisCircuit = "musers">
<cfset myFusebox.thisFuseaction = "login_user">
<cftry>
<cfoutput><cfinclude template="../model/users/act_login_users.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 19 and right(cfcatch.MissingFileName,19) is "act_login_users.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse act_login_users.cfm in circuit musers which does not exist (from fuseaction musers.login_user).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<!--- do action="vlogin.authenticate_user" --->
<cfset myFusebox.thisCircuit = "vlogin">
<cfset myFusebox.thisFuseaction = "authenticate_user">
<cftry>
<cfoutput><cfinclude template="../view/login/authenticate_user.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 21 and right(cfcatch.MissingFileName,21) is "authenticate_user.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse authenticate_user.cfm in circuit vlogin which does not exist (from fuseaction vlogin.authenticate_user).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<cfcatch><cfrethrow></cfcatch>
</cftry>

