<cfsetting enablecfoutputonly="true" />
<cfprocessingdirective pageencoding="utf-8" />
<!--- circuit: comments --->
<!--- fuseaction: postcomment --->
<cftry>
<cfset myFusebox.thisPhase = "appinit">
<cfset myFusebox.thisCircuit = "comments">
<cfset myFusebox.thisFuseaction = "postcomment">
<cfif myFusebox.applicationStart or
		not myFusebox.getApplication().applicationStarted>
	<cflock name="#application.ApplicationName#_fusebox_#FUSEBOX_APPLICATION_KEY#_appinit" type="exclusive" timeout="30">
		<cfif not myFusebox.getApplication().applicationStarted>
			<cfset myFusebox.getApplication().applicationStarted = true />
		</cfif>
	</cflock>
</cfif>
<!--- do action="vcomments.submitcomment" --->
<cfset myFusebox.thisPhase = "requestedFuseaction">
<cfset myFusebox.thisCircuit = "vcomments">
<cfset myFusebox.thisFuseaction = "submitcomment">
<cftry>
<cfoutput><cfinclude template="../view/comments/frm_post_comment.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 20 and right(cfcatch.MissingFileName,20) is "frm_post_comment.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse frm_post_comment.cfm in circuit vcomments which does not exist (from fuseaction vcomments.submitcomment).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<!--- do action="mcomments.do_post_comment" --->
<cfset myFusebox.thisCircuit = "mcomments">
<cfset myFusebox.thisFuseaction = "do_post_comment">
<cftry>
<cfoutput><cfinclude template="../model/comments/act_do_post_comment.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 23 and right(cfcatch.MissingFileName,23) is "act_do_post_comment.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse act_do_post_comment.cfm in circuit mcomments which does not exist (from fuseaction mcomments.do_post_comment).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<cfcatch><cfrethrow></cfcatch>
</cftry>

