<cfsetting enablecfoutputonly="true" />
<cfprocessingdirective pageencoding="utf-8" />
<!--- circuit: comments --->
<!--- fuseaction: deletecomment --->
<cftry>
<cfset myFusebox.thisPhase = "appinit">
<cfset myFusebox.thisCircuit = "comments">
<cfset myFusebox.thisFuseaction = "deletecomment">
<cfif myFusebox.applicationStart or
		not myFusebox.getApplication().applicationStarted>
	<cflock name="#application.ApplicationName#_fusebox_#FUSEBOX_APPLICATION_KEY#_appinit" type="exclusive" timeout="30">
		<cfif not myFusebox.getApplication().applicationStarted>
			<cfset myFusebox.getApplication().applicationStarted = true />
		</cfif>
	</cflock>
</cfif>
<!--- do action="vcomments.deletecomment" --->
<cfset myFusebox.thisPhase = "requestedFuseaction">
<cfset myFusebox.thisCircuit = "vcomments">
<cftry>
<cfoutput><cfinclude template="../view/comments/del_comment.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 15 and right(cfcatch.MissingFileName,15) is "del_comment.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse del_comment.cfm in circuit vcomments which does not exist (from fuseaction vcomments.deletecomment).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<!--- do action="mcomments.do_delete_comment" --->
<cfset myFusebox.thisCircuit = "mcomments">
<cfset myFusebox.thisFuseaction = "do_delete_comment">
<cftry>
<cfoutput><cfinclude template="../model/comments/act_do_delete_comment.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 25 and right(cfcatch.MissingFileName,25) is "act_do_delete_comment.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse act_do_delete_comment.cfm in circuit mcomments which does not exist (from fuseaction mcomments.do_delete_comment).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<cfcatch><cfrethrow></cfcatch>
</cftry>

