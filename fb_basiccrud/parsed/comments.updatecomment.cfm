<cfsetting enablecfoutputonly="true" />
<cfprocessingdirective pageencoding="utf-8" />
<!--- circuit: comments --->
<!--- fuseaction: updatecomment --->
<cftry>
<cfset myFusebox.thisPhase = "appinit">
<cfset myFusebox.thisCircuit = "comments">
<cfset myFusebox.thisFuseaction = "updatecomment">
<cfif myFusebox.applicationStart or
		not myFusebox.getApplication().applicationStarted>
	<cflock name="#application.ApplicationName#_fusebox_#FUSEBOX_APPLICATION_KEY#_appinit" type="exclusive" timeout="30">
		<cfif not myFusebox.getApplication().applicationStarted>
			<cfset myFusebox.getApplication().applicationStarted = true />
		</cfif>
	</cflock>
</cfif>
<!--- do action="vcomments.updatecomment" --->
<cfset myFusebox.thisPhase = "requestedFuseaction">
<cfset myFusebox.thisCircuit = "vcomments">
<cftry>
<cfoutput><cfinclude template="../view/comments/update_comment.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 18 and right(cfcatch.MissingFileName,18) is "update_comment.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse update_comment.cfm in circuit vcomments which does not exist (from fuseaction vcomments.updatecomment).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<!--- do action="mcomments.do_update_comment" --->
<cfset myFusebox.thisCircuit = "mcomments">
<cfset myFusebox.thisFuseaction = "do_update_comment">
<cftry>
<cfoutput><cfinclude template="../model/comments/act_do_update_comment.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 25 and right(cfcatch.MissingFileName,25) is "act_do_update_comment.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse act_do_update_comment.cfm in circuit mcomments which does not exist (from fuseaction mcomments.do_update_comment).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<cfcatch><cfrethrow></cfcatch>
</cftry>

