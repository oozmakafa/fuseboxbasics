<circuit access="internal">

    <fuseaction name="get_posts_by_user_id">
        <include template="act_get_posts_by_user_id" />
    </fuseaction>

    <fuseaction name="get_post_by_id">
        <include template="act_get_post_by_id" />
    </fuseaction>


    <fuseaction name="do_add_post">
        <include template="act_do_add_post" />
    </fuseaction>

      <fuseaction name="do_save_edit_post">
        <include template="act_do_save_edit_post" />
    </fuseaction>

     <fuseaction name="do_delete_post">
        <include template="act_do_delete_post" />
    </fuseaction>

     <fuseaction name="get_all_posts">
        <include template="act_get_all_posts" />
    </fuseaction>


</circuit>
