<circuit access="internal">

    <fuseaction name="login_user">
        <include template="act_login_users" />
    </fuseaction>

    <fuseaction name="register_user">
        <include template="act_do_submit_registration" />
    </fuseaction>

    <fuseaction name="get_all_users">
        <include template="act_get_all_users" />
    </fuseaction>

    <fuseaction name="get_users_by_email">
        <include template="act_get_users_by_email" />
    </fuseaction>

    <fuseaction name="get_users_by_username">
        <include template="act_get_users_by_username" />
    </fuseaction>

</circuit>
