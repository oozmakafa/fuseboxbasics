<circuit access="internal">


	<fuseaction name="get_comments_by_post_id">
		<include template="act_get_comments_by_post_id" />
	</fuseaction>

	<fuseaction name="do_post_comment">
		<include template="act_do_post_comment" />
	</fuseaction>

	<fuseaction name="do_delete_comment">
		<include template="act_do_delete_comment" />
	</fuseaction>

	<fuseaction name="do_update_comment">
		<include template="act_do_update_comment" />
	</fuseaction>




</circuit>
