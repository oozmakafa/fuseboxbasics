<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE circuit>
<!--
	Example circuit.xml file for the controller portion of an application.
	Only the controller circuit has public access - the controller circuit
	contains all of the fuseactions that are used in links and form posts
	within your application.
-->
<circuit access="public" xmlns:cf="cf/">
	
	<!--
		Apply a standard layout to the result of every request.
		This is fine for simple applications that have just one layout but
		for more complicated situations you will need to do something more
		advanced.
	-->
	<postfuseaction>
		<do action="layout.mainLayout" />
	</postfuseaction>
	
	<!--
		Default fuseaction for application, uses model and view circuits
		to do all of its work:
	-->



	<fuseaction name="viewuserposts">
		<do action="mposts.get_posts_by_user_id"/>
		<do action="vposts.header" contentvariable="header_content" />
		<do action="vposts.body" contentvariable="body_content"  />
		<do action="vposts.footer" contentvariable="footer_content"  />
	</fuseaction>


	<fuseaction name="viewpost">

		<do action="mposts.get_post_by_id"/>
		<do action="mcomments.get_comments_by_post_id"/>
		<do action="vposts.header" contentvariable="header_content" />
		<do action="vposts.displaypost" contentvariable="body_content"  />
		<do action="vposts.footer" contentvariable="footer_content"  />

	</fuseaction>


	<fuseaction name="viewpostid">

		<do action="mcomments.get_comments_by_post_id"/>
		
	</fuseaction>

	<fuseaction name="addpost">

		<if condition="isDefined('form.addpost')">
			<true>
				<do action="mposts.do_add_post"/>
			</true>
		</if>

		<do action="mcategories.get_categories"/>

		<do action="vposts.header" contentvariable="header_content" />
		<do action="vposts.addpost" contentvariable="body_content"  />
		<do action="vposts.footer" contentvariable="footer_content"  />

	</fuseaction>

	<fuseaction name="editpost">

		<if condition="isDefined('form.editpost')">
			<true>
				<do action="mposts.do_save_edit_post"/>
			</true>
		</if>

		<do action="mposts.get_post_by_id"/>
		<do action="mcategories.get_categories"/>
		<do action="vposts.header" contentvariable="header_content" />
		<do action="vposts.editpost" contentvariable="body_content"  />
		<do action="vposts.footer" contentvariable="footer_content"  />

	</fuseaction>

	<fuseaction name="deletepost">

		<do action="mposts.do_delete_post"/>
		<do action="vposts.header" contentvariable="header_content" />
		<do action="vposts.deletepost" contentvariable="body_content"  />
		<do action="vposts.footer" contentvariable="footer_content"  />

	</fuseaction>





		
</circuit>
