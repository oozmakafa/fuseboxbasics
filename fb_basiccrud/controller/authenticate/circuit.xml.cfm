<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE circuit>
<!--
	Example circuit.xml file for the controller portion of an application.
	Only the controller circuit has public access - the controller circuit
	contains all of the fuseactions that are used in links and form posts
	within your application.
-->
<circuit access="public" xmlns:cf="cf/">
	
	<!--
		Apply a standard layout to the result of every request.
		This is fine for simple applications that have just one layout but
		for more complicated situations you will need to do something more
		advanced.
	-->
	<postfuseaction>
		<do action="layout.mainLayout" />
	</postfuseaction>
	
	<!--
		Default fuseaction for application, uses model and view circuits
		to do all of its work:
	-->
	<fuseaction name="login">
			<if condition="isDefined('form.login')">
				<true>
					<do action="musers.login_user"/>
				</true>
			</if>


			<do action="vlogin.header" contentvariable="header_content" />
			<do action="vlogin.body" contentvariable="body_content"  />
			<do action="vlogin.footer" contentvariable="footer_content"  />

	</fuseaction>			
		
	


	<fuseaction name="logout">
		<do action="vlogin.header" contentvariable="header_content" />
		<do action="vlogin.logout" contentvariable="body_content"  />
		<do action="vlogin.footer" contentvariable="footer_content"  />
	</fuseaction>



	<fuseaction name="register">

		<if condition="isDefined('form.register')">
			<true>
				<do action="musers.get_users_by_email"/>
				<do action="musers.get_users_by_username"/>
				<do action="musers.register_user"/>
			</true>
		</if>

		<do action="vregister.header" contentvariable="header_content" />
		<do action="vregister.body" contentvariable="body_content"  />
		<do action="vregister.footer" contentvariable="footer_content"  />
	</fuseaction>






		
</circuit>
