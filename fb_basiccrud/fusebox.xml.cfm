<fusebox>

	<circuits>
        <circuit alias="home" path="controller/home/" parent="" />
        <circuit alias="vhome" path="view/home/" parent="" />
        <circuit alias="layout" path="view/layouts/" parent="" />
        <circuit alias="authenticate" path="controller/authenticate/" parent="" />
        <circuit alias="vlogin" path="view/login/" parent="" />
        <circuit alias="vregister" path="view/register/" parent="" />

        <circuit alias="musers" path="model/users" parent="" />
        <circuit alias="mposts" path="model/posts" parent="" />
        <circuit alias="mcategories" path="model/categories" parent="" />
        <circuit alias="mcomments" path="model/comments" parent="" />

        
        <circuit alias="posts" path="controller/posts" parent="" />  
        <circuit alias="vposts" path="view/posts" parent="" />

        <circuit alias="comments" path="controller/comments" parent="" />  
        <circuit alias="vcomments" path="view/comments" parent="" />

    </circuits>

	<parameters>
		<parameter name="defaultFuseaction" value="home.main" />
        <parameter name="fuseactionVariable" value="fuseaction" />
        <!-- possible values: development-circuit-load, development-full-load or production: -->
        <parameter name="mode" value="development-full-load" />
        <parameter name="conditionalParse" value="true" />
        <!-- change this to something more secure: -->
        <parameter name="password" value="skeleton" />
        <parameter name="strictMode" value="true" />
        <parameter name="debug" value="false" />
        <!-- we use the core file error templates -->
        <parameter name="errortemplatesPath" value="/fusebox5/errortemplates/" />

        <!--
            These are all default values that can be overridden:
        <parameter name="fuseactionVariable" value="fuseaction" />
        <parameter name="precedenceFormOrUrl" value="form" />
        <parameter name="scriptFileDelimiter" value="cfm" />
        <parameter name="maskedFileDelimiters" value="htm,cfm,cfml,php,php4,asp,aspx" />
        <parameter name="characterEncoding" value="utf-8" />
        <parameter name="strictMode" value="false" />
        <parameter name="allowImplicitCircuits" value="false" />
        -->
	</parameters>

	<globalfuseactions>
        <appinit>
        </appinit>

        <preprocess>
        </preprocess>

        <postprocess>

        </postprocess>
	</globalfuseactions>

    <plugins>
        <phase name="preProcess">
        </phase>
        <phase name="preFuseaction">
        </phase>
        <phase name="postFuseaction">
        </phase>
        <phase name="fuseactionException">
        </phase>
        <phase name="postProcess">
        </phase>
        <phase name="processError">
        </phase>
    </plugins>

</fusebox>
