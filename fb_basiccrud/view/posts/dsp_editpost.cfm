<CFAPPLICATION NAME="Namex" SESSIONMANAGEMENT="Yes">
<cfset title = "#get_post_by_id['title']#">
<cfset body = "#get_post_by_id['body']#">
<cfset category_id = "#get_post_by_id['category_id']#">
<cfset user_id = #session.id#>




<cfoutput>

<cfif isDefined("FORM.editpost")>
	<cflocation url="index.cfm?fuseaction=posts.viewuserposts&user_id=#user_id#">
</cfif>

<div class="container">
	<div class="post-form-wrapper">
		<h3>Edit Post</h3>
		<hr>
		<form action="" method="post">

			
		<div class="form-group">
		<label for="username">Title</label>
			<input type="text" name="title" id="title" value="#title#" class="form-control">
		</div>

		<div class="form-group">
		<label for="username">Category</label>


		<select name="category_id" class="form-control">
				<cfloop index="i" from="1" to="#get_categories.recordCount#">
					
						<cfif #category_id# EQ #get_categories['id'][i]#>
							<option value="#get_categories['id'][i]#" selected>#get_categories['category_name'][i]#</option>
						<cfelse>
							<option value="#get_categories['id'][i]#">#get_categories['category_name'][i]#</option>
						</cfif>
					
				</cfloop>
			
		</select>
		</div>

		<cfset dtNow = now()>
		<input type="hidden" name="updated_at" value="#dtNow#">
		<input type="hidden" name="user_id" value="#session.id#">
		



		<div class="form-group">
		<label for="password">Body</label>
			<textarea class="form-control" id="body" name="body">#body#</textarea>
		</div>
		<button class="btn btn-primary" type="submit" name="editpost">Save</button>

		</form>
	</div>
</div>

</cfoutput>