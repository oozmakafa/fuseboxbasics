<circuit access="internal">

    <fuseaction name="header">
        <include template="dsp_header.cfm"/>
    </fuseaction>

    <fuseaction name="body">
        <include template="dsp_body.cfm" />
    </fuseaction>

    <fuseaction name="footer">
        <include template="dsp_footer.cfm"/>
    </fuseaction>

    <fuseaction name="displaypost">
        <include template="dsp_viewpost.cfm"/>
    </fuseaction>

    <fuseaction name="editpost">
        <include template="dsp_editpost.cfm"/>
    </fuseaction>

    <fuseaction name="deletepost">
        <include template="dsp_deletepost.cfm"/>
    </fuseaction>


    <fuseaction name="addpost">
        <include template="dsp_addpost.cfm"/>
    </fuseaction>
</circuit>
