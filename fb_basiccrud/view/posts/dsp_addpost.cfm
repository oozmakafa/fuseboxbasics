<CFAPPLICATION NAME="Namex" SESSIONMANAGEMENT="Yes">

<cfif isDefined("FORM.addpost")>
	<cflocation url="index.cfm?fuseaction=posts.viewuserposts&user_id=#session.id#">
</cfif>

<div class="container">
		<div class="post-form-wrapper">
		<h3>New Post</h3>
		<hr>
		<form action="" method="post">

			
		<div class="form-group">
		<label for="username">Title</label>
		<input type="text" name="title" id="title" class="form-control">
		</div>
		<div class="form-group">
		<label for="username">Categories</label>
		<select name="category_id" class="form-control">
			<cfloop index="i" from="1" to="#get_categories.recordCount#">
				<cfoutput>
					<option value="#get_categories['id'][i]#">#get_categories['category_name'][i]#</option>
				</cfoutput>
			</cfloop>
		</select>
		</div>

		<!--- Hidden Input --->
		<cfset dtNow = now()>
		<cfset user_id = #session.id#>

		<cfoutput>
			<input type="hidden" name="user_id" value="#user_id#">
			<input type="hidden" name="created_at" value="#dtNow#">
		</cfoutput>


		<div class="form-group">
		<label for="password">Body</label>
		<textarea class="form-control" id="body" name="body"></textarea>
		</div>

		<button class="btn btn-primary" type="submit" name="addpost">Save</button>

		</form>
		</div>
</div>