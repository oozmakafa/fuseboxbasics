<CFAPPLICATION NAME="Namex" SESSIONMANAGEMENT="Yes">
<cfset title="#get_post_by_id['title']#">
<cfset body="#get_post_by_id['body']#">
<cfset dataformat="#get_post_by_id['created_at']#">
<cfset timeformat="#get_post_by_id['created_at']#">
<cfset name="#get_post_by_id['name']#">
<cfset timeformat="#get_post_by_id['created_at']#">
<cfset body="#get_post_by_id['body']#">

<div class="container view-post-style">	

	<cfoutput>
		<h1>#title#</h1>
		<p>Posted on #DateFormat("#dataformat#")# #TimeFormat("#timeformat#")# by #name#</p>
		<hr>
		<article>
			#body#
		</article>
	</cfoutput>

</div>



<!--- Comments area --->

  <div class="row">
    <div class="col-md-8">
      <h2 class="page-header">Comments</h2>
        <section class="comment-list">

        
          <!-- First Comment -->
          <cfloop index="i" from="1" to="#get_comments_by_post_id.recordCount#">
	          	<article class="row" id="article_comment">
	            <div class="col-md-2 col-sm-2 hidden-xs">
	              <figure class="thumbnail">
	                <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg" />
	                <figcaption class="text-center"><cfoutput>#get_comments_by_post_id['username'][i]#</cfoutput></figcaption>
	              </figure>
	            </div>
	            <div class="col-md-10 col-sm-10">
	              <div class="panel panel-default arrow left">
	                <div class="panel-body">
	                  <header class="text-left">
	                    <div class="comment-user"><i class="fa fa-user"></i><cfoutput>#get_comments_by_post_id['name'][i]#</cfoutput></div>
	                    <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> <cfoutput>#DateFormat("#get_comments_by_post_id['created_at'][i]#")# 
    				#TimeFormat("#get_comments_by_post_id['created_at'][i]#")#</cfoutput></time>
	                  </header>
	                  <div class="comment-post">
	                    <p>
	                      <cfoutput>#get_comments_by_post_id['comment'][i]#</cfoutput>
	                    </p>
	                   
	                  </div>
	                  <p class="text-right option_btn" ><a href="" class="btn btn-default btn-sm"><i class="fa fa-reply"></i>Reply</a></p>

	                 
	                  <cfif !isNull(#session.id#)>
	                  	
		                  <cfif #session.id# EQ #get_comments_by_post_id['user_id'][i]#>
		                  	<div  class="option_tools">
		                  			<button class="btn btn-link" data-toggle="modal" data-target="#myModal<cfoutput>#get_comments_by_post_id['id'][i]#</cfoutput>"> Edit </button>

		                  			<cfoutput>
										<div class="modal fade" id="myModal#get_comments_by_post_id['id'][i]#" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										  <div class="modal-dialog" role="document">
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										        <h4 class="modal-title" id="myModalLabel">Comment</h4>
										      </div>
										      <div class="modal-body">
										      	<div id="success_comment">
										      		<!--- form --->
										      	<form method="post" action="" class="update_comment_form">
										      		<div class="form-group">
										      			<input type="hidden" name="comment_id" value=#get_comments_by_post_id['id'][i]#> 
										      			<textarea name="comment" class="form-control">#get_comments_by_post_id['comment'][i]#</textarea>
										      		</div>
										      		<button type="submit" class="up_comment_btn" class="btn btn-primary">Update</button>
										      	</form>
										      	<!--- end form --->
										      	</div>
										      </div>
										      <div class="modal-footer">
										        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										      </div>
										    </div>
										  </div>
										</div>
		                  			</cfoutput>
									|<cfoutput> <a href="#myself##xfa.deletecomment#&id=#get_comments_by_post_id['id'][i]#" onclick="alert('Do you want really want to delete your comment?')" class="cmtdeletebtn" >Delete</a></cfoutput>
							</div>
		                  </cfif >
	                  </cfif>



	                </div>
	              </div>
	            </div>
	          </article>
          </cfloop>
        </section>

        <!-- Button trigger modal -->

        <cfif !isNull(#session.id#)>
        	

		<cfoutput>
		    <form method="post" action="" id="sbt_form">
				<textarea name="comment" class="form-control" placeholder="Comment here.."></textarea><br>
				<input type="hidden" name="user_id" value="#session.id#">
				<input type="hidden" name="post_id" value=#URL.id#>
				<button type="submit" class="btn btn-primary" name="cmtsubmit" id="cmtsubmit" >Post Comment</button>
			</form>
		</cfoutput>

    
        </cfif>

      

    </div>
  </div>
</div>
