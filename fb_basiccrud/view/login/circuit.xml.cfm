<circuit access="internal">

    <fuseaction name="header">
        <include template="dsp_header.cfm"/>
    </fuseaction>

    <fuseaction name="body">
        <include template="dsp_body.cfm" />
    </fuseaction>

    <fuseaction name="footer">
        <include template="dsp_footer.cfm"/>
    </fuseaction>

      <fuseaction name="logout">
        <include template="dsp_logout.cfm"/>
    </fuseaction>

     <fuseaction name="authenticate_user">
        <include template="authenticate_user.cfm"/>
    </fuseaction>

</circuit>
