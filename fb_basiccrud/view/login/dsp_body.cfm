<cfif !isNull(#msg_fail#)>
    <cfoutput>#msg_fail#</cfoutput>
</cfif>


<div id="msg_fail"></div>
        

  		<div class="col-md-6 col-md-offset-3 login-form-wrapper">
        <h3>Login Form</h3>
        <hr>
  			<form action="index.cfm?fuseaction=authenticate.login" method="post" class="login_form">

  				
          <div class="form-group">
            <label for="username">Username</label>
             <input type="text" name="username" id="username" class="form-control">
          </div>

          
          <div class="form-group">
            <label for="password">Password</label>
             <input type="password" name="password" id="password" class="form-control">
          </div>


				<button class="btn btn-primary" type="submit" name="login" value="true" id="login">Login</button>
        <a href="index.cfm?fuseaction=authenticate.register" class="btn btn-link">No Account yet? Create here!</a>

			</form>

</div>