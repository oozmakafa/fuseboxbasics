<div class="row-fluid top30 pagetitle">
  
  <div class="container">
    
    <div class="row">
      
      <div class="col-md-12"><h1>Blog Me</h1></div>
      
      
    </div>
    
  </div>
  
  
  
</div>
<div class="container">
  
  
  
  
  
  <div class="row">
    <div class="col-md-3">
      
      
      <h4 class="">Search</h4>
      
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
        <div class="input-group-btn">
          <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>
          </button>
        </div>
      </div>
      <button type="button" class="btn btn-default btn-sm btn-block hidden-lg hidden-md" data-toggle="collapse" data-target="#demo">Refine your search <span class="caret"></span>
        
      </button>
      
      <div id="demo" class="collapse in">
        <hr>
        <div class="list-group list-group">
          <h4 class="">Category</h4>
           <cfloop index="i" from="1" to="#get_categories.recordCount#">
          	<cfoutput>
          		<a href="" class="list-group-item"><span class="badge">12</span>#get_categories['category_name'][i]#</a>
          	</cfoutput>
          </cfloop>
       
     
        </div>
      </div>
      

      
    </div>
    <div class="col-md-9">
      
      
      <div class="well hidden-xs"> 
        
        <div class="row">
          
          <div class="col-xs-4">
            
            
            <select class="form-control">
              <option>Featured</option>
              <option>Recently Added</option>
              <option>Next Upcoming Session</option>
              <option>A-Z</option>
              <option>Z-A</option>
            </select>
          </div>
          <div class="col-xs-8">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-th"></span></button>
              <button type="button" class="btn btn-default active"><span class="glyphicon glyphicon-th-list"></span></button>
              <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-list"></span></button>
              
            </div>
          </div>
          
        </div>
        
        
        
        
        
      </div>
      
      <hr>


      <cfloop index="i" from="1" to="#get_all_posts.recordCount#">
      	<div class="row">
	        <div class="col-sm-4"><a href="#" class=""><img src="http://placehold.it/1280X720" class="img-responsive"></a>
	        </div>
	        <div class="col-sm-8">
	          <h3 class="title"><cfoutput><a href="#myself##xfa.viewpost#&id=#get_all_posts['id'][i]#">#get_all_posts['title'][i]#</a></cfoutput></h3>
	          <p class="text-muted"><span class="glyphicon glyphicon-lock"></span><cfoutput></cfoutput></p>
	          
	          <p class="text-muted">Presented by <a href="#"><cfoutput>#get_all_posts['name'][i]#</cfoutput></a></p>
	          
	        </div>
	      </div>
	      <hr>
      </cfloop>

	      
     


      
      
      
      <ul class="pagination pagination-lg pull-right">
        <li><a href="#">«</a></li>
        <li class="active"><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">»</a></li>
      </ul>
      
      
    </div>
  </div>
</div>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<script type="text/javascript">
	$(document).ready(function() {
	var showChar = 100;
	var ellipsestext = "...";
	var moretext = "more";
	var lesstext = "less";
	$('.more').each(function() {
		var content = $(this).html();

		if(content.length > showChar) {

			var c = content.substr(0, showChar);
			var h = content.substr(showChar-1, content.length - showChar);

			var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

			$(this).html(html);
		}

	});

	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
});
</script>