<circuit access="internal">

    <fuseaction name="header">
        <include template="dsp_header.cfm"/>
    </fuseaction>

    <fuseaction name="body">
        <include template="dsp_body.cfm" />
    </fuseaction>

    <fuseaction name="footer">
        <include template="dsp_footer.cfm"/>
    </fuseaction>

</circuit>
