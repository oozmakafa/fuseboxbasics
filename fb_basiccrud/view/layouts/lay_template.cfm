
 <CFAPPLICATION NAME="Namex" SESSIONMANAGEMENT="Yes">

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ColdFusion - FuseBox5</title>
    <link rel="stylesheet" href="<cfoutput>#request.cssPath#</cfoutput>/bootstrap.min.css">
    <link rel="stylesheet" href="<cfoutput>#request.cssPath#</cfoutput>/customstyle.css">
    <script type="text/javascript" src="<cfoutput>#request.jsPath#</cfoutput>jquery-3.1.0.min.js"></script>

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
  
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
   
</head>
<body>
<!---    <cfdump var="#GetHttpRequestData()#"/>--->
    
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.cfm">Basic CRUD </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

      <cfif !isNull(#session.username#)>
        <ul class="nav navbar-nav">
          <cfoutput>
            <li><a href="index.cfm">Home</a></li>
            <li><a href="#myself##xfa.viewuserposts#&user_id=#session.id#">My Post</a></li>
          </cfoutput>
        </ul>
      </cfif>
      
      <ul class="nav navbar-nav navbar-right">
      <cfif !isNull(#session.username#)>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><cfoutput>#session.username#</cfoutput><span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="index.cfm?fuseaction=authenticate.logout">Logout</a></li>
          </ul>
        </li>
      <cfelse>
        <li><a href="index.cfm?fuseaction=authenticate.login">Login</a></li>
      </cfif>
      </ul>
     
    
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


    <div class="container">
        <cfoutput>#header_content#</cfoutput>
        <cfoutput>#body_content#</cfoutput>
        <cfoutput>#footer_content#</cfoutput>
    </div>
</body>


    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="<cfoutput>#request.jsPath#</cfoutput>/ckeditor/ckeditor.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<cfoutput>#request.jsPath#</cfoutput>bootstrap.min.js"></script>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'body' );
    </script>
    <script type="text/javascript" src="<cfoutput>#request.jsPath#</cfoutput>tooltip.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('#table_id').DataTable();
      });
    </script>



  </body>
</html>


