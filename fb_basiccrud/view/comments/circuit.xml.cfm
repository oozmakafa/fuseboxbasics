<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE circuit>
<!--
    Example circuit.xml file for the layout portion of an application.
-->
<circuit access="internal">
    
    <!--
        Example layout fuseaction. The layout assumes a content variable
        called "body" has been created.
    -->
    <fuseaction name="submitcomment">
        <include template="frm_post_comment" />
    </fuseaction>

     <fuseaction name="deletecomment">
        <include template="del_comment" />
    </fuseaction>

    <fuseaction name="updatecomment">
        <include template="update_comment" />
    </fuseaction>
    
</circuit>
