<cfif !isNull(#user_exists#)>
	<div class="alert alert-danger"><cfoutput>#user_exists#</cfoutput></div>
</cfif>


<div class="col-md-6 col-md-offset-3 login-form-wrapper">
    <h3>Registration Form</h3>
    <hr>
	<form action="" method="post">

		<div class="form-group">
			<label for="name">Name</label>
			<input type="text" name="name" id="name" class="form-control">
		</div>

		<div class="form-group">
			<label for="username">Username</label>
			<input type="text" name="username" id="username" class="form-control">
		</div>

		<div class="form-group">
			<label for="email">Email</label>
			<input type="email" name="email" id="email" class="form-control">
		</div>

		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" name="password" id="password" class="form-control">
		</div>

		<div class="form-group">
			<label for="birthdate">Birthdate</label>
			<input type="date" name="birthdate" id="birthdate" class="form-control">
		</div>

			

		<button class="btn btn-primary" type="submit" name="register" >Register</button>

	</form>

</div>